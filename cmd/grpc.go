package cmd

import (
	"errors"
	"fmt"
	"bitbucket.org/marcosQuesada/common/db"
	pb "bitbucket.org/marcosQuesada/common/transport/grpc/content"
	"bitbucket.org/marcosQuesada/content-service/pkg/domain/content"
	"bitbucket.org/marcosQuesada/content-service/pkg/transport/grpc/handler"
	"google.golang.org/grpc"
	"net"
	"os"
	"os/signal"
	"syscall"

	log "github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
)

var ErrClosedConn = errors.New("use of closed network connection")

func init() {
	rootCmd.AddCommand(grpcProtoCmd)
}

var grpcProtoCmd = &cobra.Command{
	Use:   "grpc",
	Short: "starts grpc content service",
	Long:  "Starts grpc content service",
	Run: func(cmd *cobra.Command, args []string) {
		log.Infof("UserService Listening grpc calls on port %d", Config.Grpc.Port)

		db, err := db.NewPostgres(Config.Postgres)
		if err != nil {
			log.Fatalf("Fatal Initializing DB, err %v", err)
		}

		repo := content.NewSQLRepository(db)
		var svc content.Service = content.NewService(repo)
		svc = content.NewServiceMiddleware(svc)

		s := grpc.NewServer()
		pb.RegisterContentServiceServer(s, handler.NewContent(svc))

		lis, err := net.Listen("tcp", fmt.Sprintf(":%d", Config.Grpc.Port))
		if err != nil {
			log.Fatalf("failed to listen: %v", err)
		}

		c := make(chan os.Signal, 1)
		signal.Notify(c, os.Kill, os.Interrupt, syscall.SIGHUP, syscall.SIGINT, syscall.SIGTERM, syscall.SIGQUIT)
		go func() {
			<-c
			lis.Close()
		}()

		if err := s.Serve(lis); err != nil {
			if e, ok := err.(*net.OpError); ok && e.Err.Error() == ErrClosedConn.Error() {
				return
			}
			log.Errorf("Unexpected error Running server: %v", err.Error())
		}
	},
}

func GetCmd() *cobra.Command {
	return grpcProtoCmd
}

package cmd

import (
	commonCfg "bitbucket.org/marcosQuesada/common/config"
	cfg "bitbucket.org/marcosQuesada/user-service/pkg/config"
	log "github.com/sirupsen/logrus"
	"os"

	"github.com/spf13/cobra"
)

var (
	Config = cfg.Config{Postgres: commonCfg.Postgres{}, Grpc: commonCfg.Grpc{}}
)

const GrpcDefaultPort = 50051

var rootCmd = &cobra.Command{
	Use:   "content-service",
	Short: "content-service cli",
	Long:  "content-service CLI",
}

func Execute() {
	if err := rootCmd.Execute(); err != nil {
		log.Fatal(err)
		os.Exit(1)
	}
}

func init() {
	rootCmd.PersistentFlags().IntVarP(&Config.Grpc.Port, "grpc-port", "g", GrpcDefaultPort, "GRPC Port")
	rootCmd.PersistentFlags().StringVarP(&Config.Postgres.Host, "psql-host", "p", "localhost", "Postgres host")
	rootCmd.PersistentFlags().IntVarP(&Config.Postgres.Port, "psql-port", "x", 5432, "Postgres port")
	rootCmd.PersistentFlags().StringVarP(&Config.Postgres.Username, "psql-user", "u", "postgres", "Postgres user")
	rootCmd.PersistentFlags().StringVarP(&Config.Postgres.Password, "psql-pass", "s", "postgres", "Postgres pass")
	rootCmd.PersistentFlags().StringVarP(&Config.Postgres.Database, "psql-db", "d", "open-cosmos", "Postgres db")
	rootCmd.PersistentFlags().IntVarP(&Config.Postgres.MaxConnections, "psql-maxc", "c", 50, "Postgres max connections")
	rootCmd.PersistentFlags().IntVarP(&Config.Postgres.MaxConnLifetime, "psql-maxlf", "l", 1, "Postgres max life time")
}

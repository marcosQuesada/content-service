package grpc

import (
	"context"
	"fmt"
	"bitbucket.org/marcosQuesada/common/transport/grpc/content/client"
	server "bitbucket.org/marcosQuesada/content-service/cmd"
	domain "bitbucket.org/marcosQuesada/content-service/pkg/domain/content"
	"github.com/spf13/cobra"
	"testing"
)

func TestGetPublicElements(t *testing.T) {
	port := 50050
	server.Config.Grpc.Port = port
	var endpoint = fmt.Sprintf("localhost:%d", port)

	err := runServer()
	if err != nil {
		t.Fatalf("Unexpected error, %v", err)
	}

	cl, err := client.NewClient(endpoint)
	if err != nil {
		t.Fatalf("Unexpected error, %v", err)
	}

	c, err := cl.GetPublicElements(context.Background(), int(domain.PREMIUM))
	if err != nil {
		t.Fatalf("Unexpected error, %v", err)
	}

	size := 10
	if len(c) != size {
		t.Errorf("Unexpected size, expected %d got %d", size, len(c))
	}
}

func TestGetPrivateElementByID(t *testing.T) {
	port := 50051
	server.Config.Grpc.Port = port
	var endpoint = fmt.Sprintf("localhost:%d", port)

	err := runServer()
	if err != nil {
		t.Fatalf("Unexpected error, %v", err)
	}

	var callerUid = "64758166-37e6-4aa5-a7a7-7ef57f26634d"

	cl, err := client.NewClient(endpoint)
	if err != nil {
		t.Fatalf("Unexpected error, %v", err)
	}

	c, err := cl.GetPrivateElementByID(context.Background(), callerUid, 13)
	if err != nil {
		t.Fatalf("Unexpected error, %v", err)
	}

	name := "Custom3"
	if c.Title != name {
		t.Errorf("Unexpected result, expected %s got %s", name, c.Title)
	}
}

func TestGetPrivateElementByIDOnWrongOwnerID(t *testing.T) {
	port := 50052
	server.Config.Grpc.Port = port
	var endpoint = fmt.Sprintf("localhost:%d", port)

	err := runServer()
	if err != nil {
		t.Fatalf("Unexpected error, %v", err)
	}

	var callerUid = "af2acb39-5f44-4e1b-a045-41988f9b94df"

	cl, err := client.NewClient(endpoint)
	if err != nil {
		t.Fatalf("Unexpected error, %v", err)
	}

	_, err = cl.GetPrivateElementByID(context.Background(), callerUid, 13)
	if err == nil {
		t.Fatalf("Expected error, %v", err)
	}
}

func TestGetElementByOwnerID(t *testing.T) {
	port := 50053
	server.Config.Grpc.Port = port
	var endpoint = fmt.Sprintf("localhost:%d", port)

	err := runServer()
	if err != nil {
		t.Fatalf("Unexpected error, %v", err)
	}

	var callerUid = "64758166-37e6-4aa5-a7a7-7ef57f26634d"

	cl, err := client.NewClient(endpoint)
	if err != nil {
		t.Fatalf("Unexpected error, %v", err)
	}

	c, err := cl.GetPrivateElementsByOwner(context.Background(), callerUid)
	if err != nil {
		t.Fatalf("Unexpected error, %v", err)
	}

	size := 3
	if len(c) != size {
		t.Errorf("Unexpected size, expected %d got %d", size, len(c))
	}
}

func TestCreateAndDeleteElement(t *testing.T) {
	port := 50054
	server.Config.Grpc.Port = port
	var endpoint = fmt.Sprintf("localhost:%d", port)

	err := runServer()
	if err != nil {
		t.Fatalf("Unexpected error, %v", err)
	}

	cl, err := client.NewClient(endpoint)
	if err != nil {
		t.Fatalf("Unexpected error, %v", err)
	}

	e := &domain.Element{
		ID:          53,
		Title:       "fakeTitle",
		Description: "fakeDescription",
		Payload:     "{\"xxx\":\"fooBar\"}",
		Role:        domain.BASIC,
		Owner:       "fakeOnwerUUID",
		Tags:        []string{"foo", "bar"},
	}
	err = cl.CreatePrivateElement(context.Background(), e)
	if err != nil {
		t.Fatalf("Unexpected error, %v", err)
	}

	c, err := cl.GetPrivateElementByID(context.Background(), e.Owner, e.ID)
	if err != nil {
		t.Fatalf("Unexpected error, %v", err)
	}

	if c.Title != e.Title {
		t.Errorf("Unexpected Title, expected %s got %s", e.Title, c.Title)
	}

	err = cl.DeletePrivateElement(context.Background(), e.Owner, int64(e.ID))
	if err != nil {
		t.Fatalf("Unexpected error, %v", err)
	}

	c, err = cl.GetPrivateElementByID(context.Background(), e.Owner, e.ID)
	if err == nil {
		t.Fatalf("Expected error, %v", err)
	}

	if c != nil {
		t.Errorf("Expected nil, got %v", c)
	}
}

func runServer() error {
	c := server.GetCmd()

	cc := &cobra.Command{
		Use:   "content-service",
		Short: "content-service cli",
		Long:  "content-service CLI",
	}

	go c.Run(cc, []string{})

	return nil
}

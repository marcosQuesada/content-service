package test

import (
	"fmt"
	log "github.com/sirupsen/logrus"
	"github.com/gofrs/uuid"
	"path/filepath"

	"github.com/golang-migrate/migrate"
	"github.com/golang-migrate/migrate/database/postgres"
	_ "github.com/golang-migrate/migrate/database/postgres"
	_ "github.com/golang-migrate/migrate/source/file"
	"github.com/jmoiron/sqlx"
)

// NewDB creates a new ranomd DB where you can execute your tests safely
func NewDB(username, password string) (*sqlx.DB, func(), error) {
	// Generates a new random name for the DB
	uuid, err := uuid.NewV4()
	if err != nil {
		return nil, nil, err
	}
	database := uuid.String()

	// Create the DB with the random name
	dbClose, err := createDB(username, password, database)
	if err != nil {
		return nil, nil, err
	}

	// Connect to the new database
	db, err := sqlx.Connect("postgres", fmt.Sprintf("user=%s password=%s database=%s sslmode=disable", username, password, database))
	if err != nil {
		return nil, nil, err
	}

	// Load migrations to have the new random DB up to date
	migrationClose, err := loadMigrations(db)
	if err != nil {
		return nil, nil, err
	}

	// Close function to run to drop the random DB
	close := func() {
		migrationClose()
		dbClose()
	}

	return db, close, nil
}

func createDB(username, password, database string) (func(), error) {
	// Connect to postgres instance
	db, err := sqlx.Connect("postgres", fmt.Sprintf("user=%s password=%s sslmode=disable", username, password))
	if err != nil {
		return nil, err
	}

	// Creates a new database
	_, err = db.Exec(fmt.Sprintf(`CREATE DATABASE "%s" ENCODING 'UTF8';`, database))
	if err != nil {
		return nil, err
	}

	// Function that needs to be called once job is done
	close := func() {
		log.Info("CLOSE BEGIN")
		// Closes open connections except ours
		db.MustExec(fmt.Sprintf(`SELECT pg_terminate_backend(pg_stat_activity.pid) FROM pg_stat_activity WHERE pg_stat_activity.datname = '%s' AND pid <> pg_backend_pid()`, database))
		db.MustExec(fmt.Sprintf(`DROP DATABASE "%s";`, database))
		db.Close()

		log.Infof("Dropped db %s", database)
	}

	return close, err
}

func loadMigrations(db *sqlx.DB) (func(), error) {
	absPath, _ := filepath.Abs("../../../migrations/")
	migrationsPath := absPath

	driver, err := postgres.WithInstance(db.DB, &postgres.Config{})
	migration, err := migrate.NewWithDatabaseInstance("file://"+migrationsPath, "postgres", driver)
	if err != nil {
		return nil, err
	}

	cl := func() {
		migration.Close()
	}

	return cl, migration.Up()
}

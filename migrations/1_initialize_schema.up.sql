-- TABLES
CREATE TABLE catalog (
  id INTEGER PRIMARY KEY,
  title TEXT NOT NULL,
  description TEXT NOT NULL,
  payload TEXT NOT NULL,
  role INTEGER NOT NULL,
  owner TEXT,
  status BOOLEAN NOT NULL,
  updated TIMESTAMP DEFAULT current_timestamp NOT NULL,
  created TIMESTAMP DEFAULT current_timestamp NOT NULL,
  tags TEXT []
);
CREATE INDEX role_idx ON catalog USING hash (role);
CREATE INDEX tag_idx ON catalog USING gin (tags);
CREATE INDEX owner_idx ON catalog USING hash (owner);

INSERT INTO catalog (id, title, description, payload, role, owner, status, updated, created, tags)
    VALUES (1, 'Location1', 'FakeDescription', '{"latitude":"41.709829", "longitude":"-2.860610"}',0, '', true, current_timestamp, current_timestamp, '{"Tag1","Tag2"}');
INSERT INTO catalog (id, title, description, payload, role, owner, status, updated, created, tags)
    VALUES (2, 'Location2', 'FakeDescription 2', '{"latitude":"1.709829", "longitude":"-22.860610"}', 0, '', true, current_timestamp, current_timestamp, '{"Tag1","Tag3"}');
INSERT INTO catalog (id, title, description, payload, role, owner, status, updated, created, tags)
    VALUES (3, 'Location3', 'FakeDescription 3', '{"latitude":"41.709829", "longitude":"-2.860610"}',0, '', true, current_timestamp, current_timestamp, '{"Tag1","Tag2"}');
INSERT INTO catalog (id, title, description, payload, role, owner, status, updated, created, tags)
    VALUES (4, 'Location4', 'FakeDescription 4', '{"latitude":"1.709829", "longitude":"-22.860610"}',0, '', true, current_timestamp, current_timestamp, '{"Tag1","Tag3"}');
INSERT INTO catalog (id, title, description, payload, role, owner, status, updated, created, tags)
    VALUES (5, 'Location5', 'FakeDescription 5', '{"latitude":"41.709829", "longitude":"-2.860610"}',1, '', true, current_timestamp, current_timestamp, '{"Tag1","Tag2"}');
INSERT INTO catalog (id, title, description, payload, role, owner, status, updated, created, tags)
    VALUES (6, 'Location6', 'FakeDescription 6', '{"latitude":"1.709829", "longitude":"-22.860610"}', 2, '', true, current_timestamp, current_timestamp, '{"Tag1","Tag3"}');
INSERT INTO catalog (id, title, description, payload, role, owner, status, updated, created, tags)
    VALUES (7, 'Location7', 'FakeDescription 7', '{"latitude":"41.709829", "longitude":"-2.860610"}',2, '', true, current_timestamp, current_timestamp, '{"Tag1","Tag2"}');
INSERT INTO catalog (id, title, description, payload, role, owner, status, updated, created, tags)
    VALUES (8, 'Premium1', 'FakeDescription 8', '{"latitude":"1.709829", "longitude":"-22.860610"}', 3, '', true, current_timestamp, current_timestamp, '{"Tag1","Tag3"}');
INSERT INTO catalog (id, title, description, payload, role, owner, status, updated, created, tags)
    VALUES (9, 'Premium2', 'FakeDescription 9', '{"latitude":"41.709829", "longitude":"-2.860610"}', 3, '', true, current_timestamp, current_timestamp, '{"Tag1","Tag2"}');
INSERT INTO catalog (id, title, description, payload, role, owner, status, updated, created, tags)
    VALUES (10, 'Premium3', 'FakeDescription 10', '{"latitude":"1.709829", "longitude":"-22.860610"}', 3, '', true, current_timestamp, current_timestamp, '{"Tag1","Tag3"}');
INSERT INTO catalog (id, title, description, payload, role, owner, status, updated, created, tags)
    VALUES (11, 'Custom1', 'FakeDescription', '{"latitude":"1.709829", "longitude":"-22.860610"}', 0, '64758166-37e6-4aa5-a7a7-7ef57f26634d', false , current_timestamp, current_timestamp, '{"Tag1","Tag3"}');
INSERT INTO catalog (id, title, description, payload, role, owner, status, updated, created, tags)
    VALUES (12, 'Custom2', 'FakeDescription', '{"latitude":"41.709829", "longitude":"-2.860610"}', 0, '64758166-37e6-4aa5-a7a7-7ef57f26634d', false, current_timestamp, current_timestamp, '{"Tag1","Tag2"}');
INSERT INTO catalog (id, title, description, payload, role, owner, status, updated, created, tags)
    VALUES (13, 'Custom3', 'FakeDescription', '{"latitude":"13.709829", "longitude":"22.860610"}', 0, '64758166-37e6-4aa5-a7a7-7ef57f26634d', false, current_timestamp, current_timestamp, '{"Tag1","Tag3"}');

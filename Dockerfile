# Container to compile the app
FROM golang:1.11-alpine AS build

RUN mkdir -p /go/src/bitbucket.org/marcosQuesada/content-service
ADD . /go/src/bitbucket.org/marcosQuesada/content-service
WORKDIR /go/src/bitbucket.org/marcosQuesada/content-service

RUN go build -o /bin/content-service

# Final container image
FROM alpine
RUN apk --update upgrade && \
    apk add curl ca-certificates && \
    update-ca-certificates && \
    rm -rf /var/cache/apk/*
COPY --from=build /bin/content-service /bin/content-service

EXPOSE 50051
ENTRYPOINT ["/bin/content-service"]
CMD ["grpc", "--psql-host", "postgresdb"]
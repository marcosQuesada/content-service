# content-service

App Run:
```
./main grpc
```
 
Run Test Suite:
```
 go test -v ./...
```
 
 
 docker run -p 50051:50051 --network=plato_net --name content-service --rm content-service:latest

####Stack Dependency

Run Postgres database:
- docker run -it -d -p 5432:5432  --name local_postgresdb --rm -d postgres:10

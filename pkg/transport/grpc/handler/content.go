package handler

import (
	gprc "bitbucket.org/marcosQuesada/common/transport/grpc/content"
	domain "bitbucket.org/marcosQuesada/content-service/pkg/domain/content"
	"context"
	"time"
)

type Content struct {
	service domain.Service
}

func NewContent(service domain.Service) *Content {
	return &Content{service: service}
}

func (u *Content) GetPublicElements(ctx context.Context, request *gprc.PublicRequest) (*gprc.ElementsResponse, error) {
	el := make([]*gprc.Element, 0)
	elements, err := u.service.GetPublicElements(ctx, domain.RoleType(int(request.CallerRole)))
	if err != nil {
		return nil, err
	}

	for _, e := range elements {
		el = append(el, ConvertFromDomain(e))
	}

	return &gprc.ElementsResponse{Elements: el}, nil
}

func (u *Content) GetPrivateElementByID(ctx context.Context, request *gprc.ElementRequest) (*gprc.Element, error) {
	e, err := u.service.GetPrivateElementByID(ctx, request.CallerUid, int(request.ID))
	if err != nil {
		return nil, err
	}

	return ConvertFromDomain(e), nil
}

func (u *Content) GetPrivateElementsByOwner(ctx context.Context, request *gprc.ElementRequest) (*gprc.ElementsResponse, error) {
	el := make([]*gprc.Element, 0)
	elements, err := u.service.GetPrivateElementsByOwner(ctx, request.CallerUid)
	if err != nil {
		return nil, err
	}

	for _, e := range elements {
		el = append(el, ConvertFromDomain(e))
	}

	return &gprc.ElementsResponse{Elements: el}, nil
}

func (u *Content) CreatePrivateElement(ctx context.Context, request *gprc.Element) (*gprc.CreateResponse, error) {
	err := u.service.CreatePrivateElement(ctx, ConvertFromProtocol(request))
	if err != nil {
		return &gprc.CreateResponse{ID: request.ID, Success: false}, nil
	}

	return &gprc.CreateResponse{ID: request.ID, Success: true}, nil
}

func (u *Content) UpdatePrivateElement(ctx context.Context, request *gprc.UpdateElementRequest) (*gprc.UpdateResponse, error) {
	err := u.service.UpdatePrivateElement(ctx, request.CallerUid, ConvertFromProtocol(request.Element))
	if err != nil {
		return nil, err
	}

	return &gprc.UpdateResponse{ID: request.Element.ID, Success: true}, nil
}

func (u *Content) DeletePrivateElement(ctx context.Context, request *gprc.ElementRequest) (*gprc.DeleteResponse, error) {
	err := u.service.DeletePrivateElement(ctx, request.CallerUid, int(request.ID))
	if err != nil {
		return &gprc.DeleteResponse{ID: request.ID, Success: false}, nil
	}

	return &gprc.DeleteResponse{ID: request.ID, Success: true}, nil
}

func ConvertFromDomain(e *domain.Element) *gprc.Element {

	return &gprc.Element{
		ID:           int64(e.ID),
		Title:        e.Title,
		Description:  e.Description,
		Payload:      e.Payload,
		Role:         int64(e.Role),
		Owner:        e.Owner,
		PublicStatus: e.Public,
		Available:    e.Available,
		Created:      e.Created.UnixNano(),
		Updated:      e.Updated.UnixNano(),
	}
}
func ConvertFromProtocol(e *gprc.Element) *domain.Element {

	return &domain.Element{
		ID:          int(e.ID),
		Title:       e.Title,
		Description: e.Description,
		Payload:     e.Payload,
		Role:        domain.RoleType(int(e.Role)),
		Owner:       e.Owner,
		Public:      e.PublicStatus,
		Available:   e.Available,
		Created:     time.Unix(0, e.Created),
		Updated:     time.Unix(0, e.Updated),
	}
}

package content

import (
	"context"
	_ "github.com/lib/pq"
	"bitbucket.org/marcosQuesada/content-service/test" //@TODO: To common
	"testing"
	"time"
)

func TestGetAllCatalogElements(t *testing.T) {
	db, close, err := test.NewDB("postgres", "postgres")
	if err != nil {
		t.Fatalf("Error while creating a random DB for testing, err %s", err)
	}
	defer close()

	repo := NewSQLRepository(db)
	elements, err := repo.GetAll(context.Background())
	if err != nil {
		t.Fatalf("Unexpected error geting user, %v", err)
	}

	if len(elements) != 13 {
		t.Fatalf("Unexpected user lenght, expected %d got %d", 2, len(elements))
	}
}

func TestAddCatalogElement(t *testing.T) {
	db, close, err := test.NewDB("postgres", "postgres")
	if err != nil {
		t.Fatalf("Error while creating a random DB for testing, err %s", err)
	}
	defer close()

	repo := NewSQLRepository(db)
	e := &Element{
		ID:          50,
		Title:       "fakeTitle",
		Description: "fakeDescription",
		Payload:     "{\"xxx\":\"fooBar\"}",
		Role:        BASIC,
		Owner:       "fakeOnwerUUID",
		Public:      true,
		Tags:        []string{"foo", "bar"},
	}

	err = repo.Add(context.Background(), e)
	if err != nil {
		t.Fatalf("Unexpected error inserting element, %s", err)
	}

	elements, err := repo.GetAll(context.Background())
	if err != nil {
		t.Fatalf("Unexpected error geting element, %v", err)
	}

	size := 14
	if len(elements) != size {
		t.Fatalf("Unexpected user lenght, expected %d got %d", size, len(elements))
	}
}

func TestGetCatalogElementByID(t *testing.T) {
	db, close, err := test.NewDB("postgres", "postgres")
	if err != nil {
		t.Fatalf("Error while creating a random DB for testing, err %s", err)
	}
	defer close()

	repo := NewSQLRepository(db)
	el, err := repo.GetByID(context.Background(), 1)
	if err != nil {
		t.Fatalf("Unexpected error geting user by id, %s", err)
	}

	title := "Location1"
	if el.Title != title {
		t.Fatalf("Unexpected user pass, expected %s got %s", title, el.Title)
	}
}

func TestUpdatedCatalogElement(t *testing.T) {
	db, close, err := test.NewDB("postgres", "postgres")
	if err != nil {
		t.Fatalf("Error while creating a random DB for testing, err %s", err)
	}
	defer close()

	repo := NewSQLRepository(db)
	e := &Element{
		ID:          1,
		Title:       "fakeTitle",
		Description: "fakeDescription",
		Role:        BASIC,
		Owner:       "fakeOnweUUID",
		Public:      true,
		Updated:     time.Now(),
		Tags:        []string{"abc", "bar"},
	}

	err = repo.Update(context.Background(), e)
	if err != nil {
		t.Fatalf("Unexpected error inserting catalog item, %s", err)
	}

	c, err := repo.GetByID(context.Background(), e.ID)
	if err != nil {
		t.Fatalf("Unexpected error geting catalog items, %s", err)
	}

	if c.Title != e.Title {
		t.Errorf("Unexpected title content, expected %s got %s", e.Title, c.Title)
	}
}

func TestDeleteCatalogElement(t *testing.T) {
	db, close, err := test.NewDB("postgres", "postgres")
	if err != nil {
		t.Fatalf("Error while creating a random DB for testing, err %s", err)
	}
	defer close()

	repo := NewSQLRepository(db)

	err = repo.Delete(context.Background(), 1)
	if err != nil {
		t.Fatalf("Unexpected error inserting catalog item, %s", err)
	}

	elements, err := repo.GetAll(context.Background())
	if err != nil {
		t.Fatalf("Unexpected error geting catalog items, %s", err)
	}

	size := 12
	if len(elements) != size {
		t.Fatalf("Unexpected user lenght, expected %d got %d", size, len(elements))
	}
}

func TestGetCatalogElementsByTag(t *testing.T) {
	db, close, err := test.NewDB("postgres", "postgres")
	if err != nil {
		t.Fatalf("Error while creating a random DB for testing, err %s", err)
	}
	defer close()

	repo := NewSQLRepository(db)

	tag := "Tag1"
	c, err := repo.GetPublicElementsByTag(context.Background(), tag)
	if err != nil {
		t.Fatalf("Unexpected error geting catalog elements, %s", err)
	}
	if len(c) != 10 {
		t.Fatalf("Unexpected catalog elements size, got %d", len(c))
	}

	if c[0].ID != 1 {
		t.Errorf("Unexpected result, expected %d got %d", 1, c[0].ID)
	}
}

func TestGetCatalogElementsByOwner(t *testing.T) {
	db, close, err := test.NewDB("postgres", "postgres")
	if err != nil {
		t.Fatalf("Error while creating a random DB for testing, err %s", err)
	}
	defer close()

	repo := NewSQLRepository(db)

	uid := "64758166-37e6-4aa5-a7a7-7ef57f26634d"
	c, err := repo.GetByOwnerID(context.Background(), uid)
	if err != nil {
		t.Fatalf("Unexpected error geting catalog elements, %s", err)
	}

	if len(c) != 3 {
		t.Fatalf("Unexpected catalog elements size, got %d", len(c))
	}
}


func TestGetPublicElements(t *testing.T) {
	db, close, err := test.NewDB("postgres", "postgres")
	if err != nil {
		t.Fatalf("Error creating a random DB for testing, err %s", err)
	}
	defer close()

	repo := NewSQLRepository(db)

	c, err := repo.GetPublicElements(context.Background())
	if err != nil {
		t.Fatalf("Unexpected error geting catalog elements, err %s", err)
	}

	if len(c) != 10 {
		t.Fatalf("Unexpected catalog elements size ,got %d", len(c))
	}

}

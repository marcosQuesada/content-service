package content

import (
	"context"
	"testing"
	"time"
)

var fixtures = []struct {
	role 	RoleType
	fixtureResults   int
}{{BASIC, 3}, {SILVER, 6}, {GOLD, 9}, {PREMIUM, 12}}


func TestGetPublicElementsByRole(t *testing.T) {
	fakeRepo := &FakeRepository{}
	srv := NewService(fakeRepo)


	for _, value := range fixtures {
		c, err := srv.GetPublicElements(context.Background(), value.role)
		if err != nil {
			t.Fatalf("Unexpected error geting catalog elements on role %d, %s", value.role, err)
		}

		if len(c) != 12 {
			t.Errorf("Unexpected catalog elements size on role %d, got %d", value.role, len(c))
		}

		availables := 0
		for _, element := range c {
			if element.Available {
				availables++
			}
		}

		if availables != value.fixtureResults {
			t.Errorf("Unexpected available elements size on role %d, got %d", value.role, len(c))
		}
	}
}

func TestGetPrivateElementByIDOnWrongOwner(t *testing.T) {
	fakeRepo := &FakeRepository{}
	srv := NewService(fakeRepo)
	_, err := srv.GetPrivateElementByID(context.Background(), "nonExistent", 1)

	if err == nil {
		t.Fatalf("Expected error not allowed, got %v", err)
	}
}

func TestGetPrivateElementByOwnerOnWrongOwner(t *testing.T) {
	fakeRepo := &FakeRepository{}
	srv := NewService(fakeRepo)
	_, err := srv.GetPrivateElementsByOwner(context.Background(), "nonExistent")

	if err == nil {
		t.Fatalf("Expected error no element found, got %v", err)
	}
}

type FakeRepository struct {
	token string
}

func (f *FakeRepository) GetPublicElements(ctx context.Context) (Catalog, error) {
	size := 12
	elements := make([]*Element, size)
	for i:=0; i < size ; i++ {
		role := i/3
		elements[i] = &Element{
			ID:          i,
			Title:       "fakeTitle",
			Description: "fakeDescription",
			Role:        RoleType(role),
			Owner:       "fakeOnweUUID",
			Public:      true,
			Updated:     time.Now(),
			Tags:        []string{"abc", "bar"},
		}
	}

	return elements, nil
}

func (f *FakeRepository) GetByID(ctx context.Context, ID int) (*Element, error) {
	return &Element{
		ID:          1,
		Title:       "fakeTitle",
		Description: "fakeDescription",
		Role:        RoleType(0),
		Owner:       "fakeOnweUUID",
		Public:      false,
		Updated:     time.Now(),
		Tags:        []string{"abc", "bar"},
	}, nil
}

func (f *FakeRepository) GetByOwnerID(ctx context.Context, owner string) ([]*Element, error) {
	return nil, nil
}
func (f *FakeRepository) GetPublicElementsByTag(ctx context.Context, tag string) ([]*Element, error) {
	return nil, nil
}
func (f *FakeRepository) GetAll(ctx context.Context) (Catalog, error) {
	return nil, nil
}
func (f *FakeRepository) Add(ctx context.Context, element *Element) error {
	return nil
}
func (f *FakeRepository) Update(ctx context.Context, element *Element) error {
	return nil
}
func (f *FakeRepository) Delete(ctx context.Context, ID int) error {
	return nil
}

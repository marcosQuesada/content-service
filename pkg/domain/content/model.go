package content

import (
	"time"
)

type RoleType int

const (
	BASIC RoleType = iota
	SILVER
	GOLD
	PREMIUM
)

type Catalog = []*Element

type Element struct {
	ID          int      `db:"id" json:"id"`
	Title       string   `db:"title" json:"title"`
	Description string   `db:"description" json:"description"`
	Payload     string   `db:"payload" json:"payload"`
	Role        RoleType `db:"role" json:"role"`
	Owner       string   `db:"owner" json:"owner"`
	Public      bool     `db:"status" json:"status"`
	Available   bool     `json:"available"`
	Created     time.Time `db:"created" json:"created"`
	Updated     time.Time `db:"updated" json:"updated"`
	Tags        []string  `db:"tags" json:"tags"`
}

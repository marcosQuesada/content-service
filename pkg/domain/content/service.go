package content

import (
	"context"
	"errors"
)

var ErrNotAllowed = errors.New("not allowed")

type Repository interface {
	GetPublicElements(ctx context.Context) (Catalog, error)
	GetByID(ctx context.Context, ID int) (*Element, error)
	GetByOwnerID(ctx context.Context, owner string) ([]*Element, error)
	GetPublicElementsByTag(ctx context.Context, tag string) ([]*Element, error)
	GetAll(ctx context.Context) (Catalog, error)
	Add(ctx context.Context, element *Element) error
	Update(ctx context.Context, element *Element) error
	Delete(ctx context.Context, ID int) error
}

type Service interface {
	// GetPublicElements return a list of public elements with role based availability
	GetPublicElements(ctx context.Context, role RoleType) (Catalog, error)

	GetPrivateElementByID(ctx context.Context, callerUid string, ID int) (*Element, error)
	GetPrivateElementsByOwner(ctx context.Context, callerUid string) ([]*Element, error)
	CreatePrivateElement(ctx context.Context, element *Element) error
	UpdatePrivateElement(ctx context.Context, callerUid string, element *Element) error
	DeletePrivateElement(ctx context.Context, callerUid string, ID int) error
}

type DefaultService struct {
	repository Repository
}

func NewService(r Repository) *DefaultService {
	return &DefaultService{
		repository: r,
	}
}

func (s *DefaultService) GetPublicElements(ctx context.Context, callerRole RoleType) (Catalog, error) {
	c, err := s.repository.GetPublicElements(ctx)
	if err != nil {
		return nil, err
	}

	res := []*Element{}
	for _, element := range c {
		if callerRole >= element.Role {
			element.Available = true
		}
		res = append(res, element)
	}
	return res, nil
}

func (s *DefaultService) GetPublicElementsByTag(ctx context.Context, tag string) ([]*Element, error) {
	return s.repository.GetPublicElementsByTag(ctx, tag)
}

func (s *DefaultService) GetPrivateElementByID(ctx context.Context, callerUid string, ID int) (*Element, error) {
	e, err := s.repository.GetByID(ctx, int(ID))
	if err != nil {
		return nil, err
	}

	if e.Owner != callerUid {
		return nil, ErrNotAllowed
	}

	return e, nil
}

func (s *DefaultService) GetPrivateElementsByOwner(ctx context.Context, callerUid string) ([]*Element, error) {
	e, err := s.repository.GetByOwnerID(ctx, callerUid)
	if err != nil {
		return nil, err
	}

	if len(e) == 0{
		return nil, ErrNoElementFound
	}

	if e[0].Owner != callerUid {
		return nil, ErrNotAllowed
	}

	return e, nil
}

func (s *DefaultService) CreatePrivateElement(ctx context.Context, element *Element) error {
	return s.repository.Add(ctx, element)
}

func (s *DefaultService) UpdatePrivateElement(ctx context.Context, callerUid string, element *Element) error {
	e, err := s.repository.GetByID(ctx, element.ID)
	if err != nil {
		return err
	}

	if e.Owner != callerUid {
		return ErrNotAllowed
	}

	return s.repository.Update(ctx, element)
}

func (s *DefaultService) DeletePrivateElement(ctx context.Context, callerUid string, ID int) error {
	e, err := s.repository.GetByID(ctx, ID)
	if err != nil {
		return err
	}

	if e.Owner != callerUid {
		return ErrNotAllowed
	}

	return s.repository.Delete(ctx, int(ID))
}

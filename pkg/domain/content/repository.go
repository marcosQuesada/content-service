package content

import (
	"context"
	"database/sql"
	"errors"
	"fmt"
	"github.com/jmoiron/sqlx"
	"github.com/lib/pq"
	"time"
)

var ErrNoElementFound = errors.New("no element found")

type SQLRepository struct {
	db *sqlx.DB
}

func NewSQLRepository(db *sqlx.DB) *SQLRepository {
	return &SQLRepository{db: db}
}

func (r *SQLRepository) GetByID(ctx context.Context, ID int) (*Element, error) {
	const query = `SELECT id, title, description, payload,  role, owner, status, updated, created, tags FROM catalog WHERE id = $1`
	rows, err := r.db.QueryContext(ctx, query, ID)
	if err != nil {
		return nil, err
	}

	elements, err := r.populate(rows)
	if err != nil {
		return nil, err
	}
	if len(elements) == 0 {
		return nil, ErrNoElementFound
	}

	return elements[0], nil
}

func (r *SQLRepository) GetByOwnerID(ctx context.Context, owner string) ([]*Element, error) {
	const query = `SELECT id, title, description, payload,  role, owner, status, updated, created, tags FROM catalog WHERE owner = $1`
	rows, err := r.db.QueryContext(ctx, query, owner)
	if err != nil {
		return nil, err
	}

	elements, err := r.populate(rows)
	if err != nil {
		return nil, err
	}
	if len(elements) == 0 {
		return nil, ErrNoElementFound
	}

	return elements, nil
}

func (r *SQLRepository) GetPublicElementsByTag(ctx context.Context, tag string) ([]*Element, error) {
	const query = `SELECT id, title, description, payload,  role, owner, status, updated, created, tags FROM catalog WHERE status = true AND $1 = ANY(tags)`
	rows, err := r.db.QueryContext(ctx, query, tag)
	if err != nil {
		return nil, err
	}

	elements, err := r.populate(rows)
	if err != nil {
		return nil, err
	}
	if len(elements) == 0 {
		return nil, ErrNoElementFound
	}

	return elements, nil
}

func (r *SQLRepository) GetPublicElements(ctx context.Context) (Catalog, error) {
	const query = `SELECT id, title, description, payload,  role, owner, status, updated, created, tags FROM catalog WHERE status = true ORDER BY id ASC`
	rows, err := r.db.QueryContext(ctx, query)
	if err != nil {
		return nil, err
	}

	return r.populate(rows)
}

// @TODO CRUD administration (pagination & offset)
func (r *SQLRepository) GetAll(ctx context.Context) (Catalog, error) {
	const query = `SELECT id, title, description, payload,  role, owner, status, updated, created, tags FROM catalog ORDER BY id ASC`
	rows, err := r.db.QueryContext(ctx, query)
	if err != nil {
		return nil, err
	}

	return r.populate(rows)
}

func (r *SQLRepository) Add(ctx context.Context, element *Element) error {
	const query = `INSERT INTO catalog (id, title, description, payload,  role, owner, status, updated, created, tags) VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10)`
	_, err := r.db.ExecContext(ctx, query, element.ID, element.Title, element.Description, element.Payload, element.Role, element.Owner, element.Public, time.Now(), time.Now(), pq.Array(element.Tags))
	return err
}

func (r *SQLRepository) Update(ctx context.Context, element *Element) error {
	const query = `UPDATE catalog SET title=$1, description=$2, payload= $3, role=$4, owner=$5, status=$6, updated=$7, created=$8, tags=$9 WHERE id = $10`
	_, err := r.db.ExecContext(ctx, query, element.Title, element.Description, element.Payload, element.Role, element.Owner, element.Public, time.Now(), element.Created, pq.Array(element.Tags), element.ID)

	return err
}

func (r *SQLRepository) Delete(ctx context.Context, ID int) error {
	const query = `DELETE FROM catalog WHERE id = $1`
	_, err := r.db.ExecContext(ctx, query, ID)
	return err
}

func (r *SQLRepository) populate(rows *sql.Rows) (Catalog, error) {
	defer rows.Close()

	elements := []*Element{}
	for rows.Next() {
		var e = &Element{}
		if err := rows.Scan(&e.ID, &e.Title, &e.Description, &e.Payload , &e.Role, &e.Owner, &e.Public, &e.Updated, &e.Created, pq.Array(&e.Tags)); err != nil {
			return nil, fmt.Errorf("unexpected error populating catalog elements, err %s", err)
		}

		elements = append(elements, e)
	}

	if len(elements) == 0 {
		return nil, ErrNoElementFound
	}

	return Catalog(elements), nil
}

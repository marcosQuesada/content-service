package content

import (
	"context"
	log "github.com/sirupsen/logrus"
)

type ServiceMiddleware struct {
	service Service
}

func NewServiceMiddleware(srv Service) *ServiceMiddleware {
	return &ServiceMiddleware{
		service: srv,
	}
}

func (s *ServiceMiddleware) GetPublicElements(ctx context.Context, role RoleType) (Catalog, error) {
	log.Infof("GetAllElements role %d", role)

	return s.service.GetPublicElements(ctx, role)
}

func (s *ServiceMiddleware) GetPrivateElementByID(ctx context.Context, callerUid string, ID int) (*Element, error){
	log.Infof("GetPrivateElementByID callerUid %s ID %d", callerUid, ID)

	return s.service.GetPrivateElementByID(ctx, callerUid, ID)
}

func (s *ServiceMiddleware) GetPrivateElementsByOwner(ctx context.Context, callerUid string) ([]*Element, error) {
	log.Infof("GetPrivateElementsByOwner callerUid %s", callerUid)

	return s.service.GetPrivateElementsByOwner(ctx, callerUid)
}

func (s *ServiceMiddleware) CreatePrivateElement(ctx context.Context, element *Element) error {
	log.Infof("CreatePrivateElement  ID %d owner %s ",element.ID, element.Owner)

	return s.service.CreatePrivateElement(ctx, element)
}

func (s *ServiceMiddleware) UpdatePrivateElement(ctx context.Context, callerUid string, element *Element) error {
	log.Infof("UpdatePrivateElement callerUid %s ID %d owner %s ", callerUid, element.ID, element.Owner)

	return s.service.UpdatePrivateElement(ctx, callerUid, element)
}

func (s *ServiceMiddleware) DeletePrivateElement(ctx context.Context, callerUid string, ID int) error {
	log.Infof("DeletePrivateElement callerUid %s ID %d  ", callerUid, ID)

	return s.service.DeletePrivateElement(ctx, callerUid, ID)
}

package config

import (
	cfg "bitbucket.org/marcosQuesada/common/config"
)

type Config struct {
	Postgres cfg.Postgres `config:"postgress"`
	Redis    cfg.Redis    `config:"redis"`
	Grpc     cfg.Grpc     `config:"grpc"`
}
